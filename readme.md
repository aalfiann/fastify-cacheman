# fastify-cacheman
[![NPM](https://nodei.co/npm/fastify-cacheman.png?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/fastify-cacheman/)
[![js-standard-style](https://raw.githubusercontent.com/standard/standard/master/badge.svg)](https://github.com/standard/standard) 

![License](https://img.shields.io/npm/l/fastify-cacheman)
![NPM download/month](https://img.shields.io/npm/dm/fastify-cacheman.svg)
![NPM download total](https://img.shields.io/npm/dt/fastify-cacheman.svg)

Small and efficient cache provider for Fastify with In-memory, File and Redis.  
This plugin is for Fastify version 3 and 4.

> Important:  
  Fastify Cacheman since version 2.x.x is no longer include the Redis client library. Installing Redis client library is an optional.  
  If you still use NodeJS 12.x or below, you should stick with Fastify Cacheman version 2.x.x.

## Install using NPM
```bash
$ npm install fastify-cacheman
```

## Usage
**JavaScript**
```javascript
const fastify = require('fastify')()
const fastifyCacheman = require('fastify-cacheman')

fastify.register(fastifyCacheman, {
    namespace: 'fastify',
    engine: 'redis',
    client: 'redis://localhost:6379'
})

fastify.get('/', async (req, reply) => {
  await fastify.cacheman.set('tester', 'world', 120) // this will cached for 120 seconds

  reply.send({ hello: await fastify.cacheman.get('tester') })
})
```

**Typescript**
```typescript
import fastify from 'fastify'
import fastifyCacheman from 'fastify-cacheman'

fastify.register(fastifyCacheman, {
    namespace: 'fastify',
    engine: 'redis',
    client: 'redis://localhost:6379'
})

fastify.get('/', async (req, reply) => {
  await fastify.cacheman.set('tester', 'world', 120) // this will cached for 120 seconds

  reply.send({ hello: await fastify.cacheman.get('tester') })
})
```

## API

### options

```javascript
// using memory
var options = {
  engine: 'memory'
}

// using file
var options = {
  engine: 'file'
}

// using redis
var options = {
  engine: 'redis',
  port: 6379,
  host: '127.0.0.1',
  password: 'my-p@ssw0rd'
  database: 1
};

// or using redis with url connection string
// Example: redis[s]://[[username][:password]@][host][:port][/db-number]
var options = {
  engine: 'redis',
  url: 'redis://localhost:6379'
}

// or using redis client with connection string
var options = {
  engine: 'redis',
  client: 'redis://localhost:6379'
}

// or using redis client with redis instance directly
// Note:
// - Fastify Cacheman start from version 2.x.x, we excluded redis npm package,  
//   if you want to use redis client, you need to install it by yourself with run >> npm install redis@3
// - Fastify Cacheman start from version 3.x.x is no longer support redis@3, you have to use the latest redis 4.x.
var options = {
  engine: 'redis',
  client: require('redis').createClient('redis://localhost:6379')
}
```
For more details, please see [Redis 4.x Client Configuration Options](https://github.com/redis/node-redis/blob/master/docs/client-configuration.md).

### cacheman.set(key, value, [ttl, [fn]])

Stores or updates a value.

```javascript
fastify.cacheman.set('foo', { a: 'bar' }, function (err, value) {
  if (err) throw err;
  console.log(value); //-> {a:'bar'}
});
```

Or add a TTL(Time To Live) in seconds like this:

```javascript
// key will expire in 60 seconds
fastify.cacheman.set('foo', { a: 'bar' }, 60, function (err, value) {
  if (err) throw err;
  console.log(value); //-> {a:'bar'}
});
```

### cacheman.get(key, fn)

Retrieves a value for a given key, if there is no value for the given key a null value will be returned.

```javascript
fastify.cacheman.get(function (err, value) {
  if (err) throw err;
  console.log(value);
});
```

### cacheman.del(key, [fn])

Deletes a key out of the cache.

```javascript
fastify.cacheman.del('foo', function (err) {
  if (err) throw err;
  // foo was deleted
});
```

### cacheman.clear([fn])

Clear the cache entirely, throwing away all values.

```javascript
fastify.cacheman.clear(function (err) {
  if (err) throw err;
  // cache is now clear
});
```

Note:
- Support using callback, promise and async await style.
- Typescript support.

There are more spesific API for each engine type, please see:
* [recacheman](https://github.com/aalfiann/recacheman)
* [recacheman-memory](https://github.com/aalfiann/recacheman-memory)
* [recacheman-redis](https://github.com/aalfiann/recacheman-redis)
* [recacheman-file](https://github.com/aalfiann/recacheman-file)

### Unit test
```bash
npm test
```
