'use strict'

/* global describe it before after */
const server = require('fastify')
const cacheman = require('../src/index')
const assert = require('assert')

describe('cache test 3', function () {
  let app

  before(async () => {
    let environment = 'localhost'
    if (process.argv[2] === '--gitlab') {
      environment = 'redis'
    }

    app = server()

    // load routes for integration testing
    await app.register(cacheman, {
      engine: 'redis',
      client: 'redis://' + environment + ':6379'
    })
  })

  after(async () => {
    app.close()
  })

  it('cache in redis', function (done) {
    app.cacheman.set('tester', 'abc').then(function () {
      return app.cacheman.get('tester')
    }).then(function (data) {
      assert.deepEqual(data, 'abc')
      done()
    })
  })
})
