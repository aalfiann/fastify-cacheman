'use strict'

/* global describe it before */
const server = require('fastify')
const cacheman = require('../src/index')
const assert = require('assert')

describe('cache test 1', function () {
  let app

  before(async () => {
    app = server()

    // load routes for integration testing
    await app.register(cacheman, {
      engine: 'file'
    })
  })

  it('cache in file', async function () {
    await app.cacheman.set('tester', 'abc')
    assert.equal(await app.cacheman.get('tester'), 'abc')
  })
})
