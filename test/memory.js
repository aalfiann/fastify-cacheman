'use strict'

/* global describe it before */
const server = require('fastify')
const cacheman = require('../src/index')
const assert = require('assert')

describe('cache test', function () {
  let app

  before(async () => {
    app = server()

    // load routes for integration testing
    await app.register(cacheman, {
      engine: 'memory'
    })
  })

  it('cache in memory', async function () {
    await app.cacheman.set('tester', 'abc')
    assert.equal(await app.cacheman.get('tester'), 'abc')
  })
})
