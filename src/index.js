/**
 * Fastify Cacheman Plugin
 * This plugin will help you to use cacheman in fastify
 */
'use strict'

const fp = require('fastify-plugin')
const Cacheman = require('recacheman')

const defaultOptions = {
  engine: 'memory', // memory | redis | file,
  namespace: 'fastify'
}

async function fastifyCacheman (app, opts) {
  const options = Object.assign({}, defaultOptions, opts)

  if (!app.hasDecorator('cacheman')) {
    app.decorate('cacheman', new Cacheman(options.namespace, options))
  }
}

module.exports = fp(
  fastifyCacheman,
  {
    fastify: '3.x || 4.x',
    name: 'fastify-cacheman'
  }
)
module.exports.default = fastifyCacheman
module.exports.fastifyCacheman = fastifyCacheman
