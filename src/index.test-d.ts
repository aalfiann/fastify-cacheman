import Cacheman from 'recacheman'
import Fastify from 'fastify'
import { expectAssignable, expectError, expectType } from 'tsd'
import fastifyCacheman, { FastifyCachemanOptions } from './index'

const fastify = Fastify({ logger: true })

const cachemanOptions = {
  namespace: 'fastify',
  engine: 'memory',
  client: ''
};

fastify.register(fastifyCacheman, cachemanOptions);

expectAssignable<FastifyCachemanOptions>(cachemanOptions);

expectType<Cacheman>(fastify.cacheman);
expectType<Cacheman['get']>(fastify.cacheman.get);
expectType<Cacheman['set']>(fastify.cacheman.set);
expectType<Cacheman['del']>(fastify.cacheman.del);
expectType<Cacheman['clear']>(fastify.cacheman.clear);
expectType<Cacheman['cache']>(fastify.cacheman.cache);
expectType<Cacheman['use']>(fastify.cacheman.use);
expectType<Cacheman['wrap']>(fastify.cacheman.wrap);

fastify.register((app, options, done) => {
  app.get('/', async (req, reply) => {
    await app.cacheman.set('tester', 'world', 120) // this will cached for 120 seconds
    await reply.send({ hello: await app.cacheman.get('tester') })
  })
})

// We register a new instance that should trigger a typescript error.
const shouldErrorApp = Fastify({ logger: true });

const badCachemanOptions = {
  namespace: 1,
  engine: 2,
  client: ''
};

expectError(shouldErrorApp.register(fastifyCacheman, badCachemanOptions));
