import { FastifyPluginAsync } from "fastify"
import Cacheman from 'recacheman'

type FastifyCacheman = FastifyPluginAsync<fastifyCacheman.FastifyCachemanOptions>

declare module 'fastify' {
  export interface FastifyInstance {
    cacheman: Cacheman
  }
}

declare namespace fastifyCacheman {
  export interface FastifyCachemanOptions {
    engine: string,
    namespace?: string,
    client?: any,
    port?: string|number,
    host?: string,
    setex?: object,
    name?: string,
    username?: string,
    password?: string,
    database?: number,
    prefix?: string,
    url?: string,
    socket?: object
  }

  export const fastifyCacheman: FastifyCacheman
  export { fastifyCacheman as default }
}

declare function fastifyCacheman(...params: Parameters<FastifyCacheman>): ReturnType<FastifyCacheman>
export = fastifyCacheman